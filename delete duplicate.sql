create table dup as
select id
from (select DISTRICT_ID as id
from amphur
join district on amphur.AMPHUR_ID = district.AMPHUR_ID
group by amphur.AMPHUR_ID, DISTRICT_NAME
having count(*) > 1) as dup2;

delete from district
where DISTRICT_ID IN (select id from dup);

drop table dup; 