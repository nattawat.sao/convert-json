select amphur.AMPHUR_ID, amphur.AMPHUR_NAME , district.DISTRICT_NAME, count(*)
from amphur
join district on amphur.AMPHUR_ID = district.AMPHUR_ID
group by AMPHUR_ID, DISTRICT_NAME
having count(*) > 1
order by AMPHUR_ID asc
