<html>  
     <head>  
           <title>convert</title>
           <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
           <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
          <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
     </head>  
     <body>  
          <div class="container box">
          <?php
               $connect = mysqli_connect("localhost", "root", "", "thailandlocation"); //Connect PHP to MySQL Database
               mysqli_query($connect,"SET CHARACTER SET 'utf8'");
               mysqli_query($connect,"SET SESSION collation_connection ='utf8_general_ci'");
               $query_province = '';
               $query_amphur = '';
               $query_district = '';
               $table_data = '';
               $filename = "ThailandLocation.json";
               $data = file_get_contents($filename); //Read the JSON file in PHP
               $array = json_decode($data, true); //Convert JSON String into PHP Array
               foreach($array as $province)
               {
                    $query_province .= "INSERT INTO province(PROVINCE_ID, PROVINCE_CODE, PROVINCE_NAME, PROVINCE_NAME_ENG, GEO_ID) 
                               VALUES ('".$province["PROVINCE_ID"]."', '".$province["PROVINCE_CODE"]."', '".$province["PROVINCE_NAME"]."', '".$province["PROVINCE_NAME_ENG"]."', '".$province["GEO_ID"]."'); "; 
                    foreach($province['amphurs'] as $amphur)
                    {
                         $query_amphur .= "INSERT INTO amphur(AMPHUR_ID, AMPHUR_CODE, AMPHUR_NAME, AMPHUR_NAME_ENG, GEO_ID, PROVINCE_ID) 
                                     VALUES ('".$amphur["AMPHUR_ID"]."', '".$amphur["AMPHUR_CODE"]."', '".$amphur["AMPHUR_NAME"]."', '".$amphur["AMPHUR_NAME_ENG"]."', '".$amphur["GEO_ID"]."', '".$amphur["PROVINCE_ID"]."'); ";    
                         if (empty($amphur['districts'])) 
                         {
                         }
                         else
                         {
                              foreach($amphur['districts'] as $district)
                              {
                                   $query_district .= "INSERT INTO district(DISTRICT_ID, DISTRICT_CODE, DISTRICT_NAME, DISTRICT_NAME_ENG, AMPHUR_ID, PROVINCE_ID, GEO_ID) 
                                                  VALUES ('".$district["DISTRICT_ID"]."', '".$district["DISTRICT_CODE"]."', '".$district["DISTRICT_NAME"]."', '".$district["DISTRICT_NAME_ENG"]."', '".$district["AMPHUR_ID"]."', '".$district["PROVINCE_ID"]."', '".$district["GEO_ID"]."'); ";
                              }
                         }
                    }
                    ini_set('max_execution_time', 300);
               }
               if(mysqli_multi_query($connect, $query_province))
               {
                    echo '<h3 align="center">Imported Success</h3><br />';
               }

               if(mysqli_multi_query($connect, $query_amphur))
               {
                    echo '<h3 align="center">Imported Success</h3><br />';
               }

               if(mysqli_multi_query($connect, $query_district))
               {
                    echo '<h3 align="center">Imported Success</h3><br />';
               }
          ?>
          </div>  
     </body>  
 </html>